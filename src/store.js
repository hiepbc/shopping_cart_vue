import Vue from 'vue';
import Vuex from 'vuex';

import product from './store/modules/product';
import cart from './store/modules/cart';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    product,
    cart
  }
});
